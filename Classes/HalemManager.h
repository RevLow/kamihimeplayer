//
//  HalemManager.h
//  KamihimePlayer
//
//  Created by RevLow on 2016/08/15.
//
//

#ifndef __KamihimePlayer__HalemManager__
#define __KamihimePlayer__HalemManager__

#include "cocos2d.h"
#include "json11.hpp"
#include <memory>

USING_NS_CC;

struct Talk
{
    std::string charaName;
    std::string voicePath;
    std::string textWords;
    
    Talk();
};
typedef std::unique_ptr<Talk> TalkPtr;

struct Film
{
    bool autoPlay;
    bool isMovie;
    bool repeat;
    unsigned int fps;
    int whiteOutDuration;
    int stayDuration;
    std::string bgm;
    std::string texturePath;
    std::vector<TalkPtr> talk;
    
    Film();
    ~Film()
    {
        std::vector<TalkPtr>().swap(talk);
    }
};
typedef std::unique_ptr<Film> FilmPtr;

/**
 *  Jsonファイルをパーズし、データを扱うマネージャクラス
 *
 */
class HalemManager
{
public:
    static HalemManager* getInstance();
    void loadJson(const std::string& filePath);
    FilmPtr nextData();

    
    HalemManager(const HalemManager&) = delete;
    HalemManager& operator=(const HalemManager&) = delete;
    HalemManager(HalemManager&&) = delete;
    HalemManager& operator=(HalemManager&&) = delete;
private:
    HalemManager() = default;
    ~HalemManager()
    {
        std::queue<FilmPtr>().swap(_sceneData);
    };
    
    std::queue<FilmPtr> _sceneData;
    std::string _jsonLocatePath;
};

#endif /* defined(__KamihimePlayer__HalemManager__) */
