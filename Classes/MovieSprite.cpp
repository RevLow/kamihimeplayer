//
//  MovieSprite.cpp
//  KamihimePlayer
//
//  Created by RevLow on 2016/08/15.
//
//

#include "MovieSprite.h"

MovieSprite* MovieSprite::create(const std::string &textureFilePath, float interval)
{
    MovieSprite *pRet = new (std::nothrow) MovieSprite();
    if(pRet && pRet->init(textureFilePath, interval))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool MovieSprite::init(const std::string &textureFilePath, float interval)
{
    if(!Sprite::init())
    {
        return false;
    }
    
    
    _fps = interval;
    Image* img = new Image();
    img->initWithImageFile(textureFilePath);
    Texture2D* texture = new Texture2D();
    texture->initWithImage(img);
    Size size = texture->getContentSize();
    
    _XYoffset = Vec2(0, SPRITE_HEIGHT);
    _viewRect = Rect(0, size.height - SPRITE_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT);
    
    setTexture(texture);
    setTextureRect(_viewRect);
    
    img->release();
    texture->release();
    
    _isPlaying = false;
    _isRepeat = false;
    
    return true;
}

void MovieSprite::play(bool repeat)
{
    if(!_isPlaying)
    {
        _isRepeat = repeat;
        _isPlaying = true;
        float interval = 1.0f / _fps;
        schedule(schedule_selector(MovieSprite::animate), interval);
    }
}

void MovieSprite::stop()
{
    if(_isPlaying)
    {
        _isPlaying =false;
        unschedule(schedule_selector(MovieSprite::animate));
    }
}

void MovieSprite::animate(float delta)
{
    float x = _viewRect.getMinX();
    float y = _viewRect.getMinY();
    
    if(y - _viewRect.size.height < 0)
    {
        if(_isRepeat)
        {
            Size size = getTexture()->getContentSizeInPixels();
            _viewRect.setRect(x, size.height, _viewRect.size.width, _viewRect.size.height);
            y = _viewRect.getMinY();
        }
        else
        {
            _isPlaying = false;
            unschedule(schedule_selector(MovieSprite::animate));
            return;
        }
    }
    
    _viewRect.setRect(x, y - _viewRect.size.height, _viewRect.size.width, _viewRect.size.height);
    
    setTextureRect(_viewRect);
}




