//
//  HalemManager.cpp
//  KamihimePlayer
//
//  Created by RevLow on 2016/08/15.
//
//

#include "HalemManager.h"
#include "MovieSprite.h"
#include <regex>

#pragma mark - inline methods
/*!
 * パスからファイル名を取り除いたパスを抽出
 * @param[in] path パス
 * @return フォルダパス
 */
inline std::string extractFolderPath(const std::string &path)
{
    size_t pos1;
    
    pos1 = path.rfind('\\');
    if(pos1 != std::string::npos){
        return path.substr(0, pos1);
        
    }
    
    pos1 = path.rfind('/');
    if(pos1 != std::string::npos){
        return path.substr(0, pos1);
    }
    
    return "";
}

/*!
 * ファイル名から拡張子を削除
 * @param[in] fn ファイル名(フルパス or 相対パス)
 * @return フォルダパス
 */
inline std::string extractPathWithoutExt(const std::string &fn)
{
    std::string::size_type pos;
    if((pos = fn.find_last_of(".")) == std::string::npos){
        return fn;
    }
    
    return fn.substr(0, pos);
}

#pragma mark - Talk structure

Talk::Talk() :
charaName(""),
voicePath(""),
textWords("")
{
    
}

#pragma mark - Film structure

Film::Film() :
autoPlay(false),
//filmSprite(nullptr),
fps(1),
bgm(""),
texturePath(""),
isMovie(false),
whiteOutDuration(0)
{
    
}


#pragma mark - HalemManager implements

static HalemManager* instance = nullptr;

HalemManager* HalemManager::getInstance()
{
    if(instance == nullptr)
    {
        instance = new HalemManager();
    }
    
    return instance;
}

void HalemManager::loadJson(const std::string &filePath)
{
    _jsonLocatePath = extractFolderPath(filePath);
    FileUtils::getInstance()->addSearchPath(_jsonLocatePath);
    std::string jsonFileData = FileUtils::getInstance()->getStringFromFile(filePath);
    
    std::string err;
    json11::Json jsonInfo = json11::Json::parse(jsonFileData, err);
    
    for (auto &film : jsonInfo["scene_data"].array_items())
    {
        FilmPtr filmData = std::unique_ptr<Film>(new Film());
        filmData->autoPlay = film["auto"].bool_value();
        filmData->bgm = film["bgm"].is_null() ? "" : _jsonLocatePath + "/" + film["bgm"].string_value();
        filmData->fps = film["fps"].int_value();
        filmData->repeat = film["repeat"].is_null() ? true : film["repeat"].bool_value();
        
        if(film["stay"].is_null())
        {
            filmData->stayDuration = 0;
        }
        else
        {
            std::string stayStr = film["stay"].string_value();
            std::string stay = stayStr.substr(0, stayStr.length() - 1); // (秒数)+sとなっているのでsを取り除く
            filmData->stayDuration = std::stoi(stay);
        }
        
        std::string spritePath = film["film"].string_value();
        
        //black or whiteの場合以外はfullpathを代入する
        if(spritePath != "black.jpg" && spritePath != "pink_s.jpg")
        {
             //TODO: Windowsの場合は区切り文字が違うため修正が必要
            spritePath = _jsonLocatePath + "/" + extractPathWithoutExt(spritePath) + "_h.jpg";
        }
        
        filmData->isMovie = (filmData->fps != 1);
        filmData->texturePath = spritePath;
        
        //ホワイトアウトの設定
        std::string transitionString = film["transition"].is_null() ? "0" : film["transition"].string_value();
        std::regex re("\\w+(\\d+)s");
        std::smatch match;
        
        if (std::regex_match(transitionString, match, re))
        {
            filmData->whiteOutDuration = std::stoi(match[1]);
            filmData->stayDuration = std::stoi(match[1]);
        }
        
        //talkの設定
        for(auto &talkItem : film["talk"].array_items())
        {
            TalkPtr ptr = std::unique_ptr<Talk>(new Talk());
            ptr->charaName = talkItem["chara"].is_null() ? "" : talkItem["chara"].string_value();
            ptr->textWords = talkItem["words"].is_null() ? "" : talkItem["words"].string_value();
            ptr->voicePath = talkItem["voice"].is_null() ? "" : _jsonLocatePath + "/" + talkItem["voice"].string_value();
            
            filmData->talk.push_back(std::move(ptr));
        }
        
        
        _sceneData.push(std::move(filmData));
    }
}


FilmPtr HalemManager::nextData()
{
    if (_sceneData.size() == 0)
    {
        return nullptr;
    }
    FilmPtr firstData = std::move(_sceneData.front());
    _sceneData.pop();
    
    return std::move(firstData);
}