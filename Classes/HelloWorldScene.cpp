﻿#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "SimpleAudioEngine.h"

#include "OpenFileDialog/OpenFileDialog.h"

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto rootNode = CSLoader::createNode("MainScene.csb");
    rootNode->setName("rootNode");
    addChild(rootNode);
    
    
    return true;
}

void HelloWorld::onEnterTransitionDidFinish()
{
    std::unique_ptr<OpenFileDialog> ofd(new OpenFileDialog());
    ofd->setTitle("ファイルを開く");
    ofd->setFileFilter("json");
    if(ofd->showDialog())
    {
        // Jsonを読み込む
        HalemManager::getInstance()->loadJson(ofd->getFileName());
        
        //クリックイベントを登録
        auto listener = EventListenerTouchOneByOne::create();
        listener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
        this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
        
        //タッチ可能な範囲を設定
        Node* root = getChildByName("rootNode");
        Sprite* sideBar = root->getChildByName<Sprite*>("side_base_5");
        Size visibleSize = Director::getInstance()->getVisibleSize();
        _sideBarOffset = Vec2(sideBar->getContentSize().width, 0);
        _tapArea = Rect(_sideBarOffset.x, _sideBarOffset.y, visibleSize.width - _sideBarOffset.x, visibleSize.height - _sideBarOffset.y);
        ui::Text* message = root->getChildByName<ui::Text*>("MessageText");
        Sprite* talkWindow = root->getChildByName<Sprite*>("base_talk_window_2");
        message->setTextAreaSize(Size(talkWindow->getContentSize().width * 0.8, talkWindow->getContentSize().height*0.8));
        
        settingFilm();
    }
	else
	{
		exit(0);
	}
}


void HelloWorld::settingFilm()
{
    _currentFilm = HalemManager::getInstance()->nextData();
    if(_currentFilm == nullptr)
    {
        //ゲーム終了
        exit(0);
    }
    
    Sprite* replaceSprite = makeSprite(_currentFilm->texturePath, _currentFilm->fps);
    if(_currentFilm->texturePath == "pink_s.jpg")
    {
        //ホワイトアウトの処理を行う
        Sprite* whiteSprite = Sprite::create();
        whiteSprite->setTextureRect(_tapArea);
        float x = _tapArea.origin.x + _tapArea.size.width / 2.0;
        float y = _tapArea.origin.y + _tapArea.size.height / 2.0;
        whiteSprite->setPosition(x, y);
        
        whiteSprite->setColor(Color3B::WHITE);
        whiteSprite->setOpacity(0);
        
        addChild(whiteSprite, 1);
        float whiteDuration = _currentFilm->whiteOutDuration;
        auto action = Sequence::create(FadeTo::create(whiteDuration, 255), RemoveSelf::create(), NULL);
        action->setTag(1);
        whiteSprite->runAction(action);
    }
    else
    {
        //既存のスプライトが存在する場合は削除する
        Sprite* sp = getChildByName<Sprite*>("SceneSprite");
        if(sp != nullptr)
            removeChild(sp);
        Director::getInstance()->purgeCachedData();
        
        //スプライトを置き換える
        replaceSprite->setName("SceneSprite");
        float x = _tapArea.origin.x + _tapArea.size.width / 2.0;
        float y = _tapArea.origin.y + _tapArea.size.height / 2.0;
        
        replaceSprite->setPosition(x, y);
        replaceSprite->setRotation(-90);
        addChild(replaceSprite, -1);
        if(_currentFilm->isMovie)
        {
            bool repeat = _currentFilm->repeat;
            dynamic_cast<MovieSprite*>(replaceSprite)->play(repeat);
        }
    }
    
    _currentTalkIt = std::make_move_iterator(_currentFilm->talk.begin());
    
    settingTalk();
    
    if(_currentFilm->bgm != "")
    {
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(_currentFilm->bgm.c_str(), true);
    }
    
    if(_currentFilm->autoPlay)
    {
        int stay = _currentFilm->stayDuration;
        auto action = Sequence::create(DelayTime::create(stay), CallFunc::create(CC_CALLBACK_0(HelloWorld::settingFilm, this)), NULL);
        action->setTag(1);
        this->runAction(action);
    }
}

void HelloWorld::settingTalk()
{
    Node* root = getChildByName("rootNode");
    ui::Text* text = root->getChildByName<ui::Text*>("NameText");
    ui::Text* message = root->getChildByName<ui::Text*>("MessageText");
    TalkPtr p = *_currentTalkIt;
    text->setString(p->charaName);
    message->setString(p->textWords);
    if(p->voicePath != "")
    {
        CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(p->voicePath.c_str());
    }
    
    _currentTalkIt++;
}

Sprite* HelloWorld::makeSprite(const std::string& spriteName, float interval)
{
    Sprite* rtnSprite;
    if (spriteName == "black.jpg")
    {
        //黒背景
        rtnSprite = Sprite::create();
        rtnSprite->setTextureRect(_tapArea);
        rtnSprite->setColor(Color3B::BLACK);
    }
    else
    {
        //通常画像 or Movie画像
        if(interval == 1.0f)
        {
            rtnSprite = Sprite::create(spriteName);
        }
        else
        {
            rtnSprite = MovieSprite::create(spriteName, interval);
        }

    }

    return rtnSprite;
}


bool HelloWorld::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(!_tapArea.containsPoint(touch->getLocation()))
    {
        return false;
    }
    
    //2度押し防止のためアクションが動いていたら何もしないようにする
    auto actions = getActionByTag(1);
    if (actions != nullptr && !actions->isDone())
    {
        return false;
    }
    
    if(_currentTalkIt == std::make_move_iterator(_currentFilm->talk.end()))
    {
        settingFilm();
    }
    else
    {
        settingTalk();
    }
    
    return true;
}