#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

#include "HalemManager.h"
#include "MovieSprite.h"
USING_NS_CC;

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    virtual bool onTouchBegan(Touch* touch, Event* event);
    
    virtual void onEnterTransitionDidFinish();
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
private:
    Sprite* makeSprite(const std::string& spriteName, float interval);
    void settingFilm();
    void settingTalk();
    Vec2 _sideBarOffset;
    Rect _tapArea;
    FilmPtr _currentFilm;
    std::move_iterator<std::vector<TalkPtr>::iterator> _currentTalkIt;
};

#endif // __HELLOWORLD_SCENE_H__
