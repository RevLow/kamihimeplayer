//
//  OpenFileDialog.cpp
//  KamihimePlayer
//
//  Created by RevLow on 2016/07/26.
//
//

#include "OpenFileDialog.h"
#include "platform/CCPlatformConfig.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
//#include "OpenFileDialog-android.h"
#elif CC_TARGET_PLATFORM == CC_PLATFORM_MAC
#include "apple/OpenFileDialog-mac.h"
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
#include "ios/OpenFileDialog-ios.h"
#elif CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
#include "win32/OpenFileDialog-win32.h"
#elif CC_TARGET_PLATFORM == CC_PLATFORM_WINRT
//#include "OpenFileDialog-winrt.h"
#elif CC_TARGET_PLATFORM == CC_PLATFORM_LINUX
//#include "OpenFileDialog-linux.h"
#endif

OpenFileDialog::OpenFileDialog() : _platformDialog(new OpenFileDialogImpl)
{
    
}

OpenFileDialog::~OpenFileDialog()
{

}

bool OpenFileDialog::showDialog()
{
    return _platformDialog->showDialog();
}

std::string OpenFileDialog::getFileName() const
{
    return _platformDialog->getFileName();
}


void OpenFileDialog::setFileFilter(const std::string &fileFilter)
{
    _platformDialog->setFileFilter(fileFilter);
}

const std::string OpenFileDialog::getFileFilter() const
{
    return _platformDialog->getFileFilter();
}

void OpenFileDialog::setTitle(const std::string &title)
{
    _platformDialog->setTitle(title);
}

const std::string OpenFileDialog::getTitle() const
{
    return _platformDialog->getTitle();
}

void OpenFileDialog::setCanChooseDirectories(bool canChoose)
{
    _platformDialog->setCanChooseDirectories(canChoose);
}
bool OpenFileDialog::getCanChooseDirectories() const
{
    return _platformDialog->getCanChooseDirectories();
}

void OpenFileDialog::setCanCreateDirectories(bool canCreate)
{
    _platformDialog->setCanCreateDirectories(canCreate);
}

bool OpenFileDialog::getCanCreateDirectories() const
{
    return _platformDialog->getCanCreateDirectories();
}
