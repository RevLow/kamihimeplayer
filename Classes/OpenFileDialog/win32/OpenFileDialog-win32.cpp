#include "OpenFileDialog-win32.h"
#include <Windows.h>

OpenFileDialogImpl::OpenFileDialogImpl() :
	_fileName(""),
	_filterFile(""),
	_title("Open File"),
	_canChooseDirectories(false),
    _canCreateDirectories(false)
{
}

bool OpenFileDialogImpl::showDialog()
{
	OPENFILENAME ofn;
	char szFile[MAX_PATH] = "";
    ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.lpstrTitle = _title.c_str();
	ofn.lpstrFilter = TEXT(_filterFile.c_str());
	ofn.lpstrFile = szFile;
    ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_FILEMUSTEXIST;

	bool result = GetOpenFileName(&ofn);
	if (result)
	{
		_fileName = ofn.lpstrFile;
	}
	else
	{
		return false;
	}

	return true;
}

const std::string OpenFileDialogImpl::getFileName() const
{
	return _fileName;
}


void OpenFileDialogImpl::setFileFilter(const std::string &fileFilter)
{
	_filterFile = "*." + fileFilter+'\0'+ "*." + fileFilter;
}
const std::string OpenFileDialogImpl::getFileFilter() const
{
	return _filterFile;
}

void OpenFileDialogImpl::setTitle(const std::string &title)
{
	_title = title;
}
const std::string OpenFileDialogImpl::getTitle() const
{
	return _title;
}

void OpenFileDialogImpl::setCanChooseDirectories(bool canChoose)
{
	_canChooseDirectories = canChoose;
}

bool OpenFileDialogImpl::getCanChooseDirectories() const
{
	return _canChooseDirectories;
}

void OpenFileDialogImpl::setCanCreateDirectories(bool canCreate)
{
	_canCreateDirectories = canCreate;
}
bool OpenFileDialogImpl::getCanCreateDirectories() const
{
	return _canCreateDirectories;
}