//
//  OpenFileDialog.h
//  KamihimePlayer
//
//  Created by RevLow on 2016/07/26.
//
//

#ifndef KamihimePlayer_OpenFileDialog_h
#define KamihimePlayer_OpenFileDialog_h
#include <memory>
#include <string>

class OpenFileDialogImpl;

class OpenFileDialog
{
public:
    OpenFileDialog();
    ~OpenFileDialog();
    bool showDialog();
public:
    /*
     * ゲッター & セッター
     */
    
    void setFileFilter(const std::string &fileFilter);
    const std::string getFileFilter() const;
    
    void setTitle(const std::string &title);
    const std::string getTitle() const;
    
    void setCanChooseDirectories(bool canChoose);
    bool getCanChooseDirectories() const;
    
    void setCanCreateDirectories(bool canCreate);
    bool getCanCreateDirectories() const;
    std::string getFileName() const;
private:
    std::unique_ptr<OpenFileDialogImpl> _platformDialog;
    std::string _fileName;
};


#endif
