//
//  OpenFileViewController.m
//  KamihimePlayer
//
//  Created by RevLow on 2016/07/28.
//
//

#import "OpenFileViewController.h"

@interface UIImage(CommonImplement)
- (UIImage *)makeThumbnailOfSize:(float)percentage;
@end

@implementation UIImage(CommonImplement)
-(UIImage *)makeThumbnailOfSize:(float)percentage
{
    CGSize sz = CGSizeMake(self.size.width * percentage, self.size.height * percentage);
    UIGraphicsBeginImageContext(sz);
    // draw scaled image into thumbnail context
    [self drawInRect:CGRectMake(0, 0, sz.width, sz.height)];
    UIImage *newThumbnail = UIGraphicsGetImageFromCurrentImageContext();
    // pop the context
    UIGraphicsEndImageContext();
    if(newThumbnail == nil)
        NSLog(@"could not scale image");
    return newThumbnail;
}

@end

@interface OpenFileViewController ()

@end

@implementation OpenFileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem *btn =
    [[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemDone  // スタイルを指定
     target:self  // デリゲートのターゲットを指定
     action:@selector(cancelAction)  // ボタンが押されたときに呼ばれるメソッドを指定
     ];
    [self.navigationItem setRightBarButtonItem:btn animated:NO];
 
    NSFileManager *fm = [NSFileManager defaultManager];

    NSMutableArray *allFileItems = [[NSMutableArray alloc] init];
    for (NSString *path in [fm contentsOfDirectoryAtPath:_openDirectory error:nil])
    {
        //ディレクトリかチェック
        BOOL isDir;
        NSString* fullPath = [[_openDirectory stringByAppendingString:@"/"] stringByAppendingString:path];
        [fm fileExistsAtPath:fullPath isDirectory:&isDir];
        
        
        NSString *fileName = [path lastPathComponent];
        
        //もしpathのファイルがフィルタにない場合でディレクトリでもない場合
        //ファイルを追加しない
        if (_allowedFileTypes != nil &&
            ![_allowedFileTypes containsObject:[fileName pathExtension]] &&
            !isDir)
        {
            continue;
        }
        
        [allFileItems addObject:path];
    }
    _items  = (NSArray*)[allFileItems retain];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setCompletionHanlder:(std::function<void(NSString*)>)func
{
    _handler = func;
}

-(void) cancelAction
{
    _handler(@"");
    [self dismissViewControllerAnimated:YES completion:nil];
    //NSLog(@"Hello");
}



#pragma mark - Table view data source

// テーブル内のセクション数を返す
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

// テーブル内のセクション毎のデータ数を返す
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    // 配列に格納されいる文字列数を返してあげる
    return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    // セルのラベルに文字列を代入する
    cell.textLabel.text = [_items objectAtIndex:indexPath.row];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *fullpath = [[_openDirectory stringByAppendingString:@"/"] stringByAppendingString:[_items objectAtIndex:indexPath.row]];
    BOOL isDir;
    [fm fileExistsAtPath:fullpath isDirectory:&isDir];
    
    UIImage *image;
    if(isDir)
    {
       image = [UIImage imageNamed:@"ios_folder_icon.png"];
    }
    else
    {
       image = [UIImage imageNamed:@"ios_file_icon.png"];
    }
    UIImage* output = [image makeThumbnailOfSize:0.1];
    cell.imageView.image = output;

    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // セルが選択された時の処理を書く
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *fullpath = [[_openDirectory stringByAppendingString:@"/"] stringByAppendingString:[_items objectAtIndex:indexPath.row]];
    BOOL isDir;
    [fm fileExistsAtPath:fullpath isDirectory:&isDir];
    if(!isDir)
    {
       //ファイルを選択した処理
        
        //選択した文字列を引数にしたコールバックを実行
        if (_handler != nil) {
            _handler(fullpath);
        }
        //モーダルを閉じる
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        //ディレクトリを選択した処理
        //新しいモーダルを作成し、今の情報をすべて継承する
        OpenFileViewController *ofc = [[[OpenFileViewController alloc] initWithNibName:@"OpenFileViewController" bundle:nil] autorelease];
        [ofc setOpenDirectory:fullpath];
        [ofc setTitle:self.title];
        [ofc setAllowedFileTypes:_allowedFileTypes];
        if(_handler != nullptr)
            [ofc setCompletionHanlder:_handler];
        [self.navigationController pushViewController:ofc animated:YES];
    }
}

@end
