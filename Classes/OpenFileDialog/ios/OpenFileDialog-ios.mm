//
//  OpenFileDialog-ios.cpp
//  KamihimePlayer
//
//  Created by RevLow on 2016/07/28.
//
//TODO: iOSとMacの共通処理はほかのところでまとめる

#include "OpenFileDialog-ios.h"
#include "AppController.h"

#import "OpenFileViewController.h"
#import <UIKit/UIKit.h>

std::vector<std::string> split(const std::string& input, const char delimitter)
{
    std::vector<std::string> elements;
    std::string item;
    for(char c : input)
    {
        if(c == delimitter)
        {
            if(!item.empty())
            {
                elements.push_back(item);
            }
            item.clear();
        }
        else
        {
            item += c;
        }
    }
    if(!item.empty())
    {
        elements.push_back(item);
    }
    
    return elements;
}

inline NSString* convertFromStd(const std::string &str)
{
    NSString* rtnStr = [[NSString alloc] initWithUTF8String:str.c_str()];
    return rtnStr;
}

inline const std::string convertFromNSString(NSString* nsstring)
{
    return [nsstring UTF8String];
}

inline NSArray* arrayByFilter(const std::string &filter)
{
    NSMutableArray* mutableArray = [[NSMutableArray alloc] init];
    std::vector<std::string> filterVector = split(filter, '|');
    
    for(std::string &f : filterVector)
        [mutableArray addObject:convertFromStd(f)];
    
    return (NSArray *)mutableArray;
}


OpenFileDialogImpl::OpenFileDialogImpl() :
_fileName(""),
_filterFile(""),
_title("Open File"),
_canChooseDirectories(false),
_canCreateDirectories(false)
{
}

bool OpenFileDialogImpl::showDialog()
{
    OpenFileViewController *ofc = [[OpenFileViewController alloc] initWithNibName:@"OpenFileViewController" bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:ofc];
    UIViewController *appController = [UIApplication sharedApplication].keyWindow.rootViewController;

    //タイトルを設定
    [ofc setTitle:convertFromStd(_title)];
    
    //RootディレクトリをDocumentディレクトリで設定する
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    [ofc setOpenDirectory:documentsDirectoryPath];
    
    //フィルタを設定
    NSArray* allowedFileTypes = arrayByFilter(_filterFile);
    [ofc setAllowedFileTypes:allowedFileTypes];
    bool loopFinish = false;
    [ofc setCompletionHanlder:[this, &loopFinish](NSString* fileName){
        _fileName = [fileName UTF8String];
        loopFinish = true;
    }];
    
    if([appController respondsToSelector:@selector(presentViewController:animated:completion:)])
    {
        [appController presentViewController:nav animated:YES completion:nil];
    }
    
    //ofcの処理が完了するまで待つ
    while (!loopFinish)
    {
        [[NSRunLoop currentRunLoop]
         runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }

    //ファイル名が空白かパスが入っているかでtrue or falseを判定
    return _fileName != "";
}

const std::string OpenFileDialogImpl::getFileName() const
{
    return _fileName;
}


void OpenFileDialogImpl::setFileFilter(const std::string &fileFilter)
{
    _filterFile = fileFilter;
}
const std::string OpenFileDialogImpl::getFileFilter() const
{
    return _filterFile;
}

void OpenFileDialogImpl::setTitle(const std::string &title)
{
    _title = title;
}
const std::string OpenFileDialogImpl::getTitle() const
{
    return _title;
}

void OpenFileDialogImpl::setCanChooseDirectories(bool canChoose)
{
    _canChooseDirectories = canChoose;
}

bool OpenFileDialogImpl::getCanChooseDirectories() const
{
    return _canChooseDirectories;
}

void OpenFileDialogImpl::setCanCreateDirectories(bool canCreate)
{
    _canCreateDirectories = canCreate;
}
bool OpenFileDialogImpl::getCanCreateDirectories() const
{
    return _canCreateDirectories;
}