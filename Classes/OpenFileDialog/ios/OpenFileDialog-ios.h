//
//  OpenFileDialog-ios.h
//  KamihimePlayer
//
//  Created by RevLow on 2016/07/28.
//
//

#ifndef __KamihimePlayer__OpenFileDialog_ios__
#define __KamihimePlayer__OpenFileDialog_ios__

class OpenFileDialogImpl
{
public:
    OpenFileDialogImpl();
    bool showDialog();
public:
    /*
     * ゲッター & セッター
     */
    
    void setFileFilter(const std::string &fileFilter);
    const std::string getFileFilter() const;
    
    void setTitle(const std::string &title);
    const std::string getTitle() const;
    
    void setCanChooseDirectories(bool canChoose);
    bool getCanChooseDirectories() const;
    
    void setCanCreateDirectories(bool canCreate);
    bool getCanCreateDirectories() const;
    const std::string getFileName() const;
private:
    std::string _fileName;
    std::string _filterFile;
    std::string _title;
    bool _canChooseDirectories;
    bool _canCreateDirectories;
};
#endif /* defined(__KamihimePlayer__OpenFileDialog_ios__) */
