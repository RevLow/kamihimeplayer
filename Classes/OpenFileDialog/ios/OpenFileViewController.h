//
//  OpenFileViewController.h
//  KamihimePlayer
//
//  Created by RevLow on 2016/07/28.
//
//

#import <UIKit/UIKit.h>


@interface OpenFileViewController : UITableViewController
{
    NSArray *_items;
    std::function<void(NSString*)> _handler;
}
@property(nonatomic, retain) NSArray* allowedFileTypes;
@property(nonatomic, retain) NSURL* url;
@property(nonatomic, retain) NSString* openDirectory;
-(void) setCompletionHanlder:(std::function<void(NSString*)>)func;
-(void) cancelAction;
@end
