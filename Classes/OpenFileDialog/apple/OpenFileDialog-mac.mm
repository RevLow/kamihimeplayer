//
//  OpenFileDialog-mac.cpp
//  KamihimePlayer
//
//  Created by RevLow on 2016/07/27.
//
//
#import <Foundation/Foundation.h>


#include "OpenFileDialog-mac.h"

std::vector<std::string> split(const std::string& input, const char delimitter)
{
    std::vector<std::string> elements;
    std::string item;
    for(char c : input)
    {
        if(c == delimitter)
        {
            if(!item.empty())
            {
                elements.push_back(item);
            }
            item.clear();
        }
        else
        {
            item += c;
        }
    }
    if(!item.empty())
    {
        elements.push_back(item);
    }
    
    return elements;
}

inline NSString* convertFromStd(const std::string &str)
{
    NSString* rtnStr = [[NSString alloc] initWithUTF8String:str.c_str()];
    return rtnStr;
}

inline const std::string convertFromNSString(NSString* nsstring)
{
    return [nsstring UTF8String];
}

inline NSArray* arrayByFilter(const std::string &filter)
{
    NSMutableArray* mutableArray = [[NSMutableArray alloc] init];
    std::vector<std::string> filterVector = split(filter, '|');
    
    for(std::string &f : filterVector)
        [mutableArray addObject:convertFromStd(f)];
    
    return (NSArray *)mutableArray;
}

OpenFileDialogImpl::OpenFileDialogImpl() :
_fileName(""),
_filterFile(""),
_title("Open File"),
_canChooseDirectories(false),
_canCreateDirectories(false)
{
    
}

bool OpenFileDialogImpl::showDialog()
{
    NSOpenPanel* openPanel = [NSOpenPanel openPanel];
    NSArray* allowedFileTypes = arrayByFilter(_filterFile);
    [openPanel setAllowedFileTypes:allowedFileTypes];
    
    [openPanel setTitle:convertFromStd(_title)];

    NSInteger pressedButton = [openPanel runModal];
    
    if(pressedButton == NSOKButton)
    {
        NSURL *filePath = openPanel.URL;
        NSString* nsFilePath = [filePath path];
        
        _fileName = convertFromNSString(nsFilePath);
        return true;
    }
    
    return false;
}

const std::string OpenFileDialogImpl::getFileName() const
{
    return _fileName;
}

void OpenFileDialogImpl::setFileFilter(const std::string &fileFilter)
{
    _filterFile = fileFilter;
}
const std::string OpenFileDialogImpl::getFileFilter() const
{
    return _filterFile;
}

void OpenFileDialogImpl::setTitle(const std::string &title)
{
    _title = title;
}
const std::string OpenFileDialogImpl::getTitle() const
{
    return _title;
}

void OpenFileDialogImpl::setCanChooseDirectories(bool canChoose)
{
    _canChooseDirectories = canChoose;
}

bool OpenFileDialogImpl::getCanChooseDirectories() const
{
    return _canChooseDirectories;
}

void OpenFileDialogImpl::setCanCreateDirectories(bool canCreate)
{
    _canCreateDirectories = canCreate;
}
bool OpenFileDialogImpl::getCanCreateDirectories() const
{
    return _canCreateDirectories;
}