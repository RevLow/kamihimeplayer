//
//  OpenFileDialog-mac.h
//  KamihimePlayer
//
//  Created by RevLow on 2016/07/27.
//
//

#ifndef __KamihimePlayer__OpenFileDialog_mac__
#define __KamihimePlayer__OpenFileDialog_mac__

/**
 *  MacOS固有の処理を書いたOpenFileDialog
 */
class OpenFileDialogImpl
{
public:
    OpenFileDialogImpl();
    bool showDialog();
public:
    /*
     * ゲッター & セッター
     */
    const std::string getFileName() const;
    void setFileFilter(const std::string &fileFilter);
    const std::string getFileFilter() const;
    
    void setTitle(const std::string &title);
    const std::string getTitle() const;
    
    void setCanChooseDirectories(bool canChoose);
    bool getCanChooseDirectories() const;
    
    void setCanCreateDirectories(bool canCreate);
    bool getCanCreateDirectories() const;
private:
    std::string _fileName;
    std::string _filterFile;
    std::string _title;
    bool _canChooseDirectories;
    bool _canCreateDirectories;
};

#endif /* defined(__KamihimePlayer__OpenFileDialog_mac__) */
