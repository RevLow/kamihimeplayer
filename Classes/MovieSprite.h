//
//  MovieSprite.h
//  KamihimePlayer
//
//  Created by RevLow on 2016/08/15.
//
//

#ifndef __KamihimePlayer__MovieSprite__
#define __KamihimePlayer__MovieSprite__

#include "cocos2d.h"

USING_NS_CC;

#define SPRITE_WIDTH 640
#define SPRITE_HEIGHT 900

class MovieSprite : public Sprite
{
public:
    static MovieSprite* create(const std::string& textureFilePath, float interval);
    bool init(const std::string& textureFilePath, float interval);
    void play(bool repeat);
    void stop();
private:
    MovieSprite(){};
    void animate(float delta);
private:
    float _fps;
    bool _isPlaying;
    bool _isRepeat;
    Rect _viewRect;
    Vec2 _XYoffset;
};

#endif /* defined(__KamihimePlayer__MovieSprite__) */
