<GameFile>
  <PropertyGroup Name="MainScene" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="side_base_5" ActionTag="-611618698" Tag="60" IconVisible="False" LeftMargin="0.5001" RightMargin="896.4999" TopMargin="-0.8030" BottomMargin="0.8030" ctype="SpriteObjectData">
            <Size X="63.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="32.0001" Y="320.8030" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0333" Y="0.5013" />
            <PreSize X="0.0656" Y="1.0000" />
            <FileData Type="MarkedSubImage" Path="Images/side_base.png" Plist="Images/PlayUI.plist" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="base_talk_window_2" ActionTag="99948752" Tag="57" IconVisible="False" LeftMargin="63.5004" RightMargin="-0.5004" TopMargin="499.5000" BottomMargin="-0.5000" ctype="SpriteObjectData">
            <Size X="897.0000" Y="141.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="512.0004" Y="70.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5333" Y="0.1094" />
            <PreSize X="0.9344" Y="0.2203" />
            <FileData Type="MarkedSubImage" Path="Images/base_talk_window.png" Plist="Images/PlayUI.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="SoundButton" ActionTag="-1997742501" Tag="61" IconVisible="False" LeftMargin="4.1848" RightMargin="893.8152" TopMargin="2.0479" BottomMargin="571.9521" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="32" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="62.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="35.1848" Y="604.9521" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0367" Y="0.9452" />
            <PreSize X="0.0646" Y="0.1031" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="MarkedSubImage" Path="Images/btn_sound_1_0.png" Plist="Images/PlayUI.plist" />
            <PressedFileData Type="MarkedSubImage" Path="Images/btn_sound_1_0.png" Plist="Images/PlayUI.plist" />
            <NormalFileData Type="MarkedSubImage" Path="Images/btn_sound_1_0.png" Plist="Images/PlayUI.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="MessageButton" ActionTag="2074448432" Tag="62" IconVisible="False" LeftMargin="3.6445" RightMargin="895.3555" TopMargin="81.9557" BottomMargin="492.0443" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="31" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="61.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="34.1445" Y="525.0443" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0356" Y="0.8204" />
            <PreSize X="0.0635" Y="0.1031" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="MarkedSubImage" Path="Images/btn_text_0.png" Plist="Images/PlayUI.plist" />
            <PressedFileData Type="MarkedSubImage" Path="Images/btn_text_0.png" Plist="Images/PlayUI.plist" />
            <NormalFileData Type="MarkedSubImage" Path="Images/btn_text_0.png" Plist="Images/PlayUI.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ZoomButton" ActionTag="1607374177" Tag="63" IconVisible="False" LeftMargin="4.5716" RightMargin="894.4284" TopMargin="161.6822" BottomMargin="412.3178" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="31" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="61.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="35.0716" Y="445.3178" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0365" Y="0.6958" />
            <PreSize X="0.0635" Y="0.1031" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="MarkedSubImage" Path="Images/btn_zoom_0.png" Plist="Images/PlayUI.plist" />
            <PressedFileData Type="MarkedSubImage" Path="Images/btn_zoom_0.png" Plist="Images/PlayUI.plist" />
            <NormalFileData Type="MarkedSubImage" Path="Images/btn_zoom_0.png" Plist="Images/PlayUI.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="NameText" ActionTag="1045402958" Tag="64" IconVisible="False" LeftMargin="134.6127" RightMargin="778.3873" TopMargin="503.5910" BottomMargin="114.4090" FontSize="22" LabelText="名前" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="47.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="158.1127" Y="125.4090" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1647" Y="0.1960" />
            <PreSize X="0.0490" Y="0.0344" />
            <FontResource Type="Normal" Path="MTLmr3m.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="MessageText" ActionTag="-1672482775" Tag="65" IconVisible="False" LeftMargin="107.3553" RightMargin="756.6447" TopMargin="549.8763" BottomMargin="66.1237" FontSize="24" LabelText="テキスト" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="96.0000" Y="24.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position X="107.3553" Y="90.1237" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.1118" Y="0.1408" />
            <PreSize X="0.1000" Y="0.0375" />
            <FontResource Type="Normal" Path="MTLmr3m.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>